# This script should index the genome file specified in the first argument ($1),
# creating the index in a directory specified by the second argument ($2).

mkdir -p $2
STAR --runThreadN 4 --runMode genomeGenerate --genomeDir $2 --genomeFastaFiles $1 --genomeChrBinNbits 16 --genomeSAindexNbases 12 --limitSjdbInsertNsj 300000