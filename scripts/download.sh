# This script should download the file specified in the first argument ($1), place it in the directory specified in the second argument ($2), 
# and *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes".

origin=$1
destination=$2
do_gunzip=$3

mkdir -p $destination
# downloading...
wget $origin -P $destination/ -N

if [ "$do_gunzip" = "yes" ]; then
	gunzip -f -k $(find $destination -type f -name '*.gz')
fi
