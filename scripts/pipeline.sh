cd ~/decont

# Download all the files specified in data/filenames
echo "Downloading reads..."  >> log/pipeline.log
wget -i data/urls -P data/reads/ -N

# Download the contaminants fasta file, and uncompress it
echo "Downloading contaminants..."  >> log/pipeline.log
contaminants_url='https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz'
bash scripts/download.sh $contaminants_url res yes

# Index the contaminants file
echo
echo "Running STAR index for contaminants..."  >> log/pipeline.log
if test -e res/contaminants_idx
then
    echo 'info: output directory already exists, so it will be overridden' >> log/pipeline.log
fi
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx

# Merge the samples into a single file
mkdir -p out/merged
sampleids=$(ls data/reads/*.fastq.gz | cut -d "_" -f1-2 | cut -d "/" -f3 | cut -d "-" -f1 | sort | uniq)
for sid in $sampleids
do
	echo "Merging reads for "$sid"..."  >> log/pipeline.log
    bash scripts/merge_fastqs.sh data/reads out/merged $sid
done

# Run cutadapt for all merged files
mkdir -p out/trimmed
mkdir -p log/cutadapt
for sid in $sampleids
do
	if test -e out/trimmed/$sid.trimmed.fastq.gz
	then
	    echo 'info: output file 'out/trimmed/$sid.trimmed.fastq.gz' already exists, so it will be overridden' >> log/pipeline.log
	fi
	echo "Running cutadapt for "$sid"..."  >> log/pipeline.log
	cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/$sid.trimmed.fastq.gz out/merged/$sid.fastq.gz > log/cutadapt/$sid.log
	grep -i -E 'reads with adapters|total basepairs processed' log/cutadapt/$sid.log | sed 's/^/\t\t\t/'>> log/pipeline.log
done

# Run STAR for all trimmed files
for fname in out/trimmed/*.fastq.gz
do
    sid=$(basename $fname | cut -d "." -f1)
    mkdir -p out/star/$sid
    if test -e out/star/$sid/Aligned.out.sam
	then
	    echo 'info: output file 'out/star/$sid/Aligned.out.sam' already exists, so it will be overridden' >> log/pipeline.log
	fi
    echo "Running STAR mapping for "$sid"..."  >> log/pipeline.log
    STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/$sid.trimmed.fastq.gz --readFilesCommand gunzip -c --outFileNamePrefix out/star/$sid/
    grep -i -E 'uniquely mapped reads %|% of reads mapped to multiple loci|% of reads mapped to too many loci' out/star/$sid/Log.final.out >> log/pipeline.log
done

echo -e "Done at $(date +"%H:%M:%S")\n"  >> log/pipeline.log
